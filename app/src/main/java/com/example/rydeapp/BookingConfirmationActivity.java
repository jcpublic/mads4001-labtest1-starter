/* -----------------------------------
 * Put your student name and ID here
 * ----------------------------------- */

package com.example.rydeapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.rydeapp.R;

public class BookingConfirmationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_confirmation);
    }
}
